#!/bin/bash

# you need to set PACKET_PROJECT_ID

IPS=$(packet device get --project-id $PACKET_PROJECT_ID --json | jq 'sort_by(.hostname) | .[] | {(.hostname): {dest: "packet", ip: [(.ip_addresses[] | .address)]}}' | jq -s add -c)

cat <<EOF > templates/secrets.yaml
apiVersion: v1
kind: Secret
metadata:
  name: {{ .Release.Name }}-packet-ips
  labels:
    app: {{ .Chart.Name }}
    chart: "{{ .Chart.Name }}-{{ .Chart.Version }}"
    release: "{{ .Release.Name }}"
    heritage: "{{ .Release.Service }}"
stringData:
  packet_ips.json: |-
    $IPS
EOF
