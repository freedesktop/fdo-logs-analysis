# fdo-logs-analysis

tools and scripts used to analyse the gitlab logs

Note: this is now a helm chart, so use helm to deploy it:

```bash
kubectl create namespace monitoring
helm --namespace monitoring upgrade --install logs .
```

## dump the ci postresql as CSV

### create a deploy key for https://gitlab.freedesktop.org/freedesktop/fdo-containers-usage

To be able to push the data to the git repo, we need to create a deploy key:

```bash
ssh-keygen -t ed25519 -f fdo-container-usages.key -N ""
ssh-keyscan gitlab.freedesktop.org > known_hosts
kubectl --namespace monitoring create secret generic fdo-ci-dump-ssh-key --from-file=fdo-container-usages.key=fdo-container-usages.key --from-file=known_hosts=known_hosts
```
