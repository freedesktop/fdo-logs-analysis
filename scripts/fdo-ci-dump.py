#!/bin/env python3

import click
import csv
import git
import json
import os
import psycopg2
import sys
import yaml
from collections import namedtuple
from datetime import datetime, timedelta
from google.cloud import storage
from pathlib import Path

# the time we start pulling data
start_time = datetime.now() - timedelta(hours=1)

# uncomment to re-parse all of the logs
# start_time = datetime.fromisoformat('2020-02-27')

filename_prefix = 'ci_dump-'

ssh_env = {}


def round_time(t):
    ts = int(t.timestamp())
    ts -= ts % 3600
    return datetime.fromtimestamp(ts)


def get_time_range(start_time):
    now = datetime.now()
    end_time = start_time + timedelta(hours=1)

    # compute the range
    if (now - start_time).days > 0:
        # we are not fetching today's data

        if start_time.month != now.month:
            start_time = start_time.replace(day=1, hour=0)
            if start_time.month == 12:
                end_time = start_time.replace(year=start_time.year + 1, month=1)
            else:
                end_time = start_time.replace(month=start_time.month + 1)
        else:
            start_time = start_time.replace(hour=0)
            end_time = start_time + timedelta(days=1)

    return start_time, end_time


def get_week_time_range(start_time):
    midnight = start_time - timedelta(hours=start_time.hour, minutes=start_time.minute)
    monday = midnight - timedelta(days=start_time.weekday())
    end_time = monday + timedelta(days=7)
    return monday, end_time


def get_jobs_options_query(start_time, end_time):
    return f'''SELECT
    ci_builds.id,
    ci_builds.status,
    namespaces."path" as namespace,
    projects."path",
    ci_builds.created_at,
    ci_pipelines.sha,
    ci_stages.pipeline_id,
    ci_pipelines.iid as pipeline_iid,
    ci_builds_metadata.config_options,
    ci_builds_metadata.config_variables
FROM
    ci_builds, ci_builds_metadata, namespaces, projects, ci_pipelines, ci_stages
    where
        ci_builds.id = ci_builds_metadata.build_id
        and
        ci_builds.status != 'skipped'
        and
        ci_builds.project_id = projects.id
        and
        projects.namespace_id = namespaces.id
        and
        ci_builds.stage_id = ci_stages.id
        and
        ci_stages.pipeline_id = ci_pipelines.id
        and
        ci_builds.created_at >= '{start_time}'
          and
          ci_builds.created_at < '{end_time}'
    order by id
'''


def get_jobs_query(start_time, end_time):
    return f'''SELECT
    ci_builds.id,
    ci_stages.pipeline_id,
    ci_builds."name",
    ci_stages.name as stage,
    ci_builds.status,
    ci_builds.created_at,
    ci_builds.queued_at,
    ci_builds.started_at,
    ci_builds.finished_at,
    ci_builds.user_id,
    ci_builds.runner_id
FROM
    ci_builds, ci_stages
    where
      ci_builds.stage_id = ci_stages.id
      and
      ci_builds.created_at >= '{start_time}'
          and
          ci_builds.created_at < '{end_time}'
    order by id
'''


def get_pipelines_query(start_time, end_time):
    return f'''SELECT
    ci_pipelines.id,
    ci_pipelines.project_id,
    ci_pipelines.status,
    ci_pipelines.created_at,
    ci_pipelines.started_at,
    ci_pipelines.finished_at
FROM
    ci_pipelines
    where
      ci_pipelines.created_at >= '{start_time}'
          and
          ci_pipelines.created_at < '{end_time}'
    order by id
'''


def get_projects_query():
    return '''SELECT
    projects.id,
    namespaces."path" as namespace,
    projects."path"
FROM
    projects, namespaces
    where
      projects.namespace_id = namespaces.id
    order by id'''


def get_runners_query():
    return '''SELECT
    ci_runners.id,
    ci_runners.description
FROM
    ci_runners
    order by id'''


def get_users_query():
    return '''SELECT
    users.id,
    users.name
FROM
    users
'''


queries = {
    'jobs': get_jobs_query,
    'job_options': get_jobs_options_query,
    'pipelines': get_pipelines_query,
    'projects': get_projects_query,
    'runners': get_runners_query,
    'users': get_users_query,
}


def get_prefix(kind):
    return f'ci_{kind}_dump'


def fetch_psql(conn, query, prefix, start_time, end_time, dry_run):
    cur = conn.cursor()
    outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)

    timestamp = ''

    # the other logs are offset by one hour, keep it consistent
    if start_time is not None:
        timestamp = '-' + (start_time - timedelta(hours=1)).strftime('%Y_%m_%d_%H_00_00')
    result_file = f'{prefix}{timestamp}.csv'

    if dry_run:
        print(f'query {prefix} between {start_time} and {end_time}')
    else:
        with open(result_file, 'w') as f:
            cur.copy_expert(outputquery, f)
    cur.close()

    return result_file


def cleanup(bucket, conn, kind, start_time, dry_run):
    prefix = get_prefix(kind)

    fetched_months = []
    fetched_days = []

    for blob in bucket.list_blobs(prefix=prefix + '-'):
        blob_date = blob.name[len(prefix + '-'):][:19]
        blob_datetime = datetime.strptime(blob_date, '%Y_%m_%d_%H_%M_%S') + timedelta(hours=1)

        if blob_datetime.year != start_time.year:
            # ignore too old files
            continue

        if blob_datetime.month != start_time.month:
            # different month: we need to keep only one file
            if blob_datetime.day == 1:
                continue

            # we have more than one file for this month: cleanup all of the others
            # and do a one month query:

            if dry_run:
                print(f'would clean up "{blob.name}"')
            else:
                print(f'cleaning up "{blob.name}"')
                blob.delete()

            if blob_datetime.month not in fetched_months:

                start_time, end_time = get_time_range(blob_datetime)
                query = queries[kind](start_time, end_time)

                result_file = fetch_psql(conn, query, prefix, start_time, end_time, dry_run)

                print(result_file)

                if not dry_run:
                    blob = storage.Blob(result_file, bucket)
                    blob.upload_from_filename(result_file)

                fetched_months.append(blob_datetime.month)
            continue

        # we should be on the same month/year here
        if blob_datetime.day != start_time.day:
            # different day: we need to keep only one file
            if blob_datetime.hour == 0:
                continue

            # we have more than one file for this day: cleanup all of the others
            # and do a one day query:

            if dry_run:
                print(f'would clean up "{blob.name}"')
            else:
                print(f'cleaning up "{blob.name}"')
                blob.delete()

            if blob_datetime.day not in fetched_days:

                _start_time, _end_time = get_time_range(blob_datetime)

                query = queries[kind](_start_time, _end_time)

                result_file = fetch_psql(conn, query, prefix, _start_time, _end_time, dry_run)

                print(result_file)

                if not dry_run:
                    blob = storage.Blob(result_file, bucket)
                    blob.upload_from_filename(result_file)

                fetched_days.append(blob_datetime.day)


def parse_job_options(csvfile_path, start_time, repo):
    filename = Path(repo.working_dir) / f'images_{start_time.strftime("%Y-%m-%d")}.yaml'

    images = {}
    with open(csvfile_path) as csvfile:
        data = csv.DictReader(csvfile, delimiter=',', quotechar='"')
        for row in data:
            if not row['config_options']:
                continue

            job = namedtuple('LogPoint', row.keys())(*row.values())

            if '"image":' not in job.config_options:
                continue

            options = json.loads(job.config_options)

            image = options['image']['name']

            if '$' in image:
                old_env = os.environ
                registry = 'registry.freedesktop.org'
                os.environ = {
                    'CI_REGISTRY': registry,
                    'CI_PROJECT_NAME': job.path,
                    'CI_PROJECT_NAMESPACE': job.namespace,
                    'CI_PROJECT_PATH': '/'.join((job.namespace, job.path)),
                    'CI_REGISTRY_IMAGE': '/'.join((registry, job.namespace, job.path)),
                    'CI_COMMIT_SHA': job.sha,
                    'CI_PIPELINE_ID': job.pipeline_id,
                    'CI_PIPELINE_IID': job.pipeline_iid,
                    'CI_TEMPLATE_REGISTRY_HOST': 'registry.gitlab.com',
                }
                variables = json.loads(job.config_variables)
                for v in variables:
                    os.environ[v['key']] = v['value']

                max_loop = 5
                while max_loop and '$' in image:
                    image = os.path.expandvars(image)
                    max_loop -= 1

                os.environ = old_env

            if '$' in image:
                print(job.namespace, job.path, job.id, job.created_at, options['image']['name'], image)
                print('                 ', variables)

            i = images.get(image, {'last_used_at': job.created_at, 'count': 0})
            i['count'] += 1

            if i['last_used_at'] < job.created_at:
                i['last_used_at'] = job.created_at

            # i = images.get(image, {'last_used_at': job.created_at, 'jobs': []})
            # i['jobs'].append('/'.join((job.namespace, job.path, job.id)))

            images[image] = i

    with open(filename, 'w') as f:
        f.write(yaml.dump(images, Dumper=yaml.CDumper))

    repo.git.add(filename)


@click.command()
@click.option('--dry-run', default=False, is_flag=True, help='Do not fetch the sql DB nor create/delete files.')
@click.option('--start-time', default=str(round_time(start_time)), help='since when do we need to start')
@click.option('--kind', multiple=True, default=['users', 'projects', 'runners', 'jobs', 'pipelines', 'job_options'], help='kind of objects to dump (can be used multiple times)')
@click.option('--usage-repo-url', default='git@gitlab.freedesktop.org:freedesktop/fdo-containers-usage.git', help='the distant repo to push the image files')
@click.option('--usage-repo', default='fdo-containers-usage', type=click.Path(file_okay=False, dir_okay=True, writable=True), help='the directory where we cloned the distant repo to push the image usages')
@click.option('--identity', default='/etc/ssh-key/fdo-container-usages.key', type=click.Path(file_okay=True, dir_okay=False), help='the private key to forward to ssh when using git on the distant repo (default to `/etc/ssh-key/fdo-container-usages.key`)')
def main(dry_run, start_time, kind, usage_repo_url, usage_repo, identity):
    start_time = round_time(datetime.fromisoformat(start_time))
    actual_start_time = start_time
    initial_start_time = start_time
    client = storage.Client.from_service_account_json('/etc/creds/upload_gcs_key')
    bucket = storage.Bucket(client, 'fdo-gitlab-storage-transfer-logs')

    identity = Path(identity)

    if not identity.exists():
        print('Please provide a ssh private key so we can push to the distant repo')
        sys.exit(1)

    git_ssh_cmd = f'ssh -i {identity}'

    global ssh_env

    ssh_env['GIT_SSH_COMMAND'] = git_ssh_cmd

    usage_repo = Path(usage_repo)
    if not usage_repo.exists():
        git.Repo.clone_from(usage_repo_url, usage_repo, env=ssh_env, depth=10)

    repo = git.Repo(usage_repo)
    repo.config_writer().set_value("user", "name", "fdo-bot").release()
    repo.config_writer().set_value("user", "email", "fdo-bot@example.org").release()

    with repo.git.custom_environment(GIT_SSH_COMMAND=ssh_env['GIT_SSH_COMMAND']):
        origin = repo.remote('origin')
        origin.pull(depth=10)

    password = None

    do_kinds = kind

    with open('/etc/creds/postgres_password') as f:
        password = f.readline().strip()
    conn = psycopg2.connect(dbname='gitlab_production', user='gitlab', password=password, host='gitlab-prod-postgresql-postgresql.gitlab.svc')

    for kind in ['users', 'projects', 'runners']:
        if kind not in do_kinds:
            continue
        query = queries[kind]()
        result_file = fetch_psql(conn, query, get_prefix(kind), None, None, dry_run)

        print(result_file)

        if not dry_run:
            blob = storage.Blob(result_file, bucket)
            blob.upload_from_filename(result_file)

    for kind in ['jobs', 'pipelines']:
        if kind not in do_kinds:
            continue
        cleanup(bucket, conn, kind, start_time, dry_run)

    while start_time < datetime.now():
        start_time, end_time = get_time_range(start_time)

        for kind in ['jobs', 'pipelines']:
            if kind not in do_kinds:
                continue
            query = queries[kind](start_time, end_time)
            result_file = fetch_psql(conn, query, get_prefix(kind), start_time, end_time, dry_run)
            if start_time < actual_start_time:
                actual_start_time = start_time

            print(result_file)

            if not dry_run:
                blob = storage.Blob(result_file, bucket)
                blob.upload_from_filename(result_file)

        start_time = end_time

    start_time = initial_start_time
    while start_time < datetime.now():
        start_time, end_time = get_week_time_range(start_time)
        for kind in ['job_options']:
            if kind not in do_kinds:
                continue
            query = queries[kind](start_time, end_time)
            result_file = fetch_psql(conn, query, get_prefix(kind), start_time, end_time, dry_run)

            print(result_file)

            parse_job_options(result_file, start_time, repo)

        start_time = end_time

    # commit and push the results
    if 'job_options' in do_kinds:
        repo.index.commit('automated update of files')
        with repo.git.custom_environment(GIT_SSH_COMMAND=ssh_env['GIT_SSH_COMMAND']):
            origin = repo.remote('origin')
            origin.push()


if __name__ == '__main__':
    main()
