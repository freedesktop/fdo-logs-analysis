#!/bin/env python3

import click
import geoip2.database
import influxdb
import json
import re
import requests
import shlex
import sys
import threading
import yaml

from concurrent.futures import TimeoutError
from datetime import datetime, timedelta
from geolib import geohash
from google.cloud import pubsub_v1
from pathlib import Path

# we will buffer the data for 5 min then send them as a batch
step = 60 * 5

project_id = "fdo-gitlab"
subscription_name = "nginx_logs"
states_data = {}
countries_data = {}
countries = {}

DB_NAME = 'GCP'


upstreams = {
    '[default-gitlab-prod-gitlab-8005]': 'webhorse',
    '[default-gitlab-prod-webservice-8181]': 'webhorse',
    '[default-gitlab-prod-webservice-default-8181]': 'webhorse',
    '[default-gitlab-prod-gitlab-8105]': 'registry',
    '[default-gitlab-prod-registry-5000]': 'registry',
    '[default-gitlab-prod-gitlab-8090]': 'pages',
}


def kib(bytes):
    return bytes / 1024


def mib(bytes):
    return kib(bytes) / 1024


def gib(bytes):
    return mib(bytes) / 1024


def bytes_to_human(bytes):
    for fun, suffix in [(gib, 'GB'),
                        (mib, 'MB'),
                        (kib, 'KB'),
                        ]:
        v = fun(float(bytes))
        if v > 1:
            return f'{v:.2f} {suffix}'

    return str(bytes)


def get_correct_iso_format(date):
    date = date.replace('Z', '')

    if len(date) > 26:
        date = date[:26]

    # if the time in microseconds is not represented by 6 numbers,
    # datetime complains.
    return date.ljust(26, '0') if '.' in date else date


def get_date(date_str):
    return datetime.fromisoformat(get_correct_iso_format(date_str)) if date_str else None


def get_correct_round_time(date):
    return round_time(get_date(date)) if date else None


def round_time(t, step=3600):
    ts = int(t.timestamp())
    ts -= ts % step
    return datetime.fromtimestamp(ts)


class OneHourOfRollingData(object):
    singleton = None

    def __init__(self):
        self.slots = {}
        self.lock = threading.Lock()
        OneHourOfRollingData.singleton = self

    @classmethod
    def get_data_object(cls):
        if cls.singleton is not None:
            return cls.singleton
        return OneHourOfRollingData()

    @classmethod
    def send_to_influxdb(cls, not_after_datetime):
        points = []
        global_data = cls.get_data_object()
        not_after_timestamp = not_after_datetime.timestamp()
        send_objects = [(ts, global_data.slots[ts]) for ts in global_data.slots if ts <= not_after_timestamp]

        for timestamp, slot in send_objects:
            ts = datetime.fromtimestamp(timestamp).isoformat()
            for obj in slot.values():
                obj['time'] = ts
                print(ts, obj['tags'], bytes_to_human(obj['fields']['value']))

                points.append(obj)

            del(global_data.slots[timestamp])

        idb.write_points(points)

        return points

    @classmethod
    def add_data(cls, measurement, tags, fields, timestamp):
        ts = timestamp.timestamp()

        slot_key = int(ts - ts % step)

        obj = cls.get_data_object()

        obj.lock.acquire()

        slot = obj.slots.get(slot_key, {})

        item = slot.get(str(tags), {
            'measurement': measurement,
            'tags': tags,
            'fields': {},
        })
        for k, v in fields.items():
            item['fields'][k] = item['fields'].get(k, 0) + v

        slot[str(tags)] = item
        obj.slots[slot_key] = slot

        obj.lock.release()


def setup_geoip_db(geodb):
    global geoipdb, states_data, countries_data, countries

    geoipdb = geoip2.database.Reader(geodb)

    # construct the map US state/country name -> 2 letters worldmap code
    for kind, glob_obj in (('states', states_data), ('countries', countries_data)):
        filepath = Path(f'{kind}.yaml')
        if not filepath.exists():
            data = requests.get(f'https://raw.githubusercontent.com/grafana/worldmap-panel/master/src/data/{kind}.json').json()
            glob_obj = dict([(s['name'], s) for s in data])

            # special case where we get 'District of Columbia' instead of 'Washington DC'
            if 'Washington DC' in glob_obj:
                glob_obj['District of Columbia'] = glob_obj['Washington DC']

            # now compute the geohash tags for the various lat/long
            for entry in glob_obj.values():
                entry['geohash'] = geohash.encode(entry['latitude'], entry['longitude'], 8)

            with open(filepath, 'w') as f:
                yaml.safe_dump(glob_obj, f)
        else:
            with open(filepath) as f:
                glob_obj.update(yaml.safe_load(f))

    countries = dict(((s['key'], s) for s in countries_data.values()))

    # fix South Sudan
    countries['SS'] = countries['SD']


dest_regexes = [
    ('packet', re.compile(r'^147.75.(6[4-9]|[78]\d|9[0-5]).\d*$')),
    ('packet_arm', re.compile(r'^139.178.(6[4-9]|[78]\d|9[0-5]).\d*$')),
    ('packet_ipv6', re.compile(r'^2604:1380:.*$')),
    ('hetzner', re.compile(r'^95.21[67].*')),
    ('hetzner_ipv6', re.compile(r'^2a01:4f9:4a:318d:.*$')),
    ('hetzner_ipv6', re.compile(r'^2a01:4f8:.*$')),
    ('local', re.compile(r'^(34.74.69.21|35.243.208.232)$')),
]


def get_geo_tags(dest_ip):
    dest = 'internet'

    for k, r in dest_regexes:
        if r.match(dest_ip):
            dest = k

    tags = {'dest': dest}

    # assume one of the public VM ips in case we are talking to the local network
    if dest_ip.startswith('10.'):
        dest_ip = '34.74.69.21'
        tags['dest'] = 'local'

    try:
        geo = geoipdb.city(dest_ip)
    except geoip2.errors.AddressNotFoundError:
        c = 'AQ'  # Antarctica
        tags['geohash'] = countries[c]['geohash']
        tags['geo_code'] = countries[c]['key']
        tags['country'] = countries[c]['name']
        print(f'ERROR: ip {dest_ip} not found in database', file=sys.stderr)
    else:
        if geo.country.iso_code == 'US':
            geo_tag = geo.subdivisions.most_specific.name
            tags['geohash'] = states_data[geo_tag]['geohash']
            tags['geo_code'] = states_data[geo_tag]['key']
            tags['country'] = geo.country.name
        else:
            if geo.country.iso_code not in countries:
                c = 'AQ'  # Antarctica
                tags['geohash'] = countries[c]['geohash']
                tags['geo_code'] = countries[c]['key']
                tags['country'] = countries[c]['name']
                print(f'ERROR: country {geo.country} not found in database', file=sys.stderr)
            else:
                tags['geohash'] = countries[geo.country.iso_code]['geohash']
                tags['geo_code'] = geo.country.iso_code
                tags['country'] = countries[geo.country.iso_code]['name']

    return tags


def process_log(data):
    # silently ignore messages without textPayload (they have jsonPayload
    # and are generated by k8s, not nginx)
    if 'textPayload' not in data:
        return

    log = shlex.split(data['textPayload'])

    # silently ignore too short messages (TCP*)
    if len(log) < 3:
        return

    # example of data:
    # ['xxx.yyy.zzz.aaaa', '-', '-', '[03/Jun/2020:15:06:34', '+0000]', 'GET /plymouth/plymouth/-/issues/114 HTTP/2.0', '200', '14409', '-', 'Mozilla/5.0 (X11; Linux x86_64; rv:77.0) Gecko/20100101 Firefox/77.0', '39', '0.185', '[default-gitlab-prod-gitlab-8005]', '[]', '10.0.0.92:8005', '70761', '0.185', '200', 'aff83ea1a6846861cd5f05a40053b26a']
    # Compared to the docs, we are shifted by one because the date of the query gets split in 2
    # https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/log-format/
    # 0 -> $remote_addr
    # 3/4 -> $time_local
    # 5 -> $request
    # 6 -> $status
    # 7 -> $body_bytes_sent
    # 8 -> $http_referer
    # 9 -> $http_user_agent
    # 10 -> $request_length
    # 11 -> $request_time
    # 12 -> [$proxy_upstream_name] or [$proxy_alternative_upstream_name]
    # 13 -> []
    # 14 -> $upstream_addr
    # 15 -> $upstream_response_length
    # 16 -> $upstream_response_time
    # 17 -> $upstream_status
    # 18 -> $req_id

    # silently ignore messages of type:
    # '2020/06/05 10:17:20 [warn] 5526#5526: *57311821 a client request body is buffered to a temporary file /tmp/client-body/0000065194, client: xxx.xxx.xxx.xxx, server: gitlab-ci-packet-old-dgc.freedesktop.org, request: "PATCH /api/v4/jobs/2971546/trace HTTP/1.1", host: "gitlab-ci-packet-old-dgc.freedesktop.org"
    # or
    # '2020/06/05 16:48:18 [error] 5265#5265: *58753438 upstream prematurely closed connection while reading upstream, client: xxx.xxx.xxx.xxx, server: gitlab.freedesktop.org, request: "GET /api/v4/jobs/2976603/artifacts HTTP/1.1", upstream: "http://10.0.0.92:8005/api/v4/jobs/2976603/artifacts", host: "gitlab.freedesktop.org"
    if log[2] in ('[warn]', '[error]'):
        return

    if len(log) != 19:
        print(f'    **** can not process {data}')
        return

    # silently ignore logs where we do not have egress
    if log[7] == '0':
        return

    dest_ip = log[0]
    request = log[5]
    try:
        status = int(log[6])
        sent_bytes = int(log[7])
    except ValueError:
        print(f'    **** can not process {data}')
        return
    upstream = log[12]

    if upstream in upstreams:
        upstream = upstreams[upstream]

    try:
        method, url, protocol = request.split(' ')
    except ValueError:
        print(f'suspicious log: {data["textPayload"]}')
        return

    tags = get_geo_tags(dest_ip)
    tags['kind'] = 'nginx'
    tags['method'] = method
    tags['service'] = upstream
    tags['status'] = status

    timestamp = get_date(data['timestamp'])

    if upstream == 'webhorse':
        path = url.strip('/')
        if '?' in url:
            path, args = path.split('?', 1)

        splitted_url = path.split('/')

        if splitted_url[-1] == 'artifacts':
            tags['subkind'] = splitted_url[-1]

        elif splitted_url[0] in ('api', 'assets', 'uploads'):
            tags['subkind'] = splitted_url[0]

        else:
            tags['subkind'] = 'web'
            tags['namespace'] = splitted_url[0]

            if len(splitted_url) >= 2:
                project = splitted_url[1].replace('.git', '')
                tags['project'] = project

            if 'git-upload-pack' in url:
                tags['subkind'] = 'git'

    OneHourOfRollingData.add_data('sent_bytes_sum', tags, {'value': sent_bytes}, timestamp)
    # print(timestamp, sent_bytes, dest_ip, tags, url)


def callback(message):
    data = json.loads(message.data)
    process_log(data)
    message.ack()


@click.command()
@click.option('--dry-run', default=False, is_flag=True, help='Do not inject data in the InfluxDB.')
@click.option('--database', default='logs-influxdb.monitoring', help='the database to contact')
@click.option('--geodb',
              default='dbip-city-lite-2020-06.mmdb',
              type=click.Path(file_okay=True, dir_okay=False),
              help='the geoip database')
def main(dry_run, database, geodb):
    global idb, geoipdb, states_data, countries_data, countries

    idb = influxdb.InfluxDBClient(database, 8086, 'root', 'root', DB_NAME)

    # ensures the database exists
    if DB_NAME not in [db['name'] for db in idb.get_list_database()]:
        idb.create_database(DB_NAME)

    setup_geoip_db(geodb)

    subscriber = pubsub_v1.SubscriberClient()
    # The `subscription_path` method creates a fully qualified identifier
    # in the form `projects/{project_id}/subscriptions/{subscription_name}`
    subscription_path = subscriber.subscription_path(project_id, subscription_name)

    # instanciate once the data object (because multithreading)
    OneHourOfRollingData.get_data_object()

    print("Listening for messages on {}..\n".format(subscription_path))
    streaming_pull_future = subscriber.subscribe(subscription_path, callback=callback)

    # Wrap subscriber in a 'with' block to automatically call close() when done.
    count = 0
    with subscriber:
        try:
            while True:
                try:
                    # When `timeout` is not set, result() will block indefinitely,
                    # unless an exception is encountered first.
                    streaming_pull_future.result(timeout=60)
                except TimeoutError:
                    count += 1

                OneHourOfRollingData.send_to_influxdb(datetime.now() - timedelta(minutes=5))

        except KeyboardInterrupt:
            streaming_pull_future.cancel()


if __name__ == '__main__':
    main()
