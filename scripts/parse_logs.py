#!/bin/env python3

import base64
import click
import crcmod
import csv
import geoip2.database
import influxdb
import json
import re
import requests
import struct
import sys
import time
import yaml

from collections import namedtuple
from datetime import datetime, timedelta
from geolib import geohash
from google.cloud import storage
from pathlib import Path

SLEEP_INTERVAL = 300

# we will buffer the data for 5 min then send them as a batch
step = 60 * 5

# we need to load the past x months of ci data
ci_dump_months_offset = 1

# the time we start sending data again (a little bit more than 36 hours)
start_time = datetime.now() - timedelta(hours=36)

# uncomment to re-parse all of the logs
# start_time = datetime.fromisoformat('2020-02-27')

# uncomment to re-pare the last 2 days
# start_time = datetime.now() - timedelta(days=2)

start_timestamp = None

runners = {}
users = {}
projects = {}
states_data = {}
countries_data = {}
countries = {}
known_ips = {}

DB_NAME = 'GCP'


def kib(bytes):
    return bytes / 1024


def mib(bytes):
    return kib(bytes) / 1024


def gib(bytes):
    return mib(bytes) / 1024


def bytes_to_human(bytes):
    for fun, suffix in [(gib, 'GB'),
                        (mib, 'MB'),
                        (kib, 'KB'),
                        ]:
        v = fun(float(bytes))
        if v > 1:
            return f'{v:.2f} {suffix}'

    return str(bytes)


def get_correct_iso_format(date):
    # if the time in microseconds is not represented by 6 numbers,
    # datetime complains.
    return date.ljust(26, '0') if '.' in date else date


def get_correct_round_time(date):
    return round_time(datetime.fromisoformat(get_correct_iso_format(date))) if date else None


def download_blob_if_needed(blob):
    p = Path(blob.name)

    if p.exists():
        crc32c = crcmod.predefined.Crc('crc-32c')
        with open(p, 'rb') as f:
            content = f.read()
            crc32c.update(content)
            if crc32c.crcValue == struct.unpack('>I', base64.b64decode(blob.crc32c))[0]:
                return False

    print(f'downloading {blob.name} from bucket')
    blob.download_to_filename(blob.name)
    return True


class Job(object):
    all_jobs = {}

    def __init__(self, id, pipeline, stage, created_at, started_at, finished_at, status):
        self.id = int(id)
        self.pipeline = pipeline
        self.stage = stage
        self.created_at = get_correct_round_time(created_at)
        self.started_at = get_correct_round_time(started_at)
        self.finished_at = get_correct_round_time(finished_at)
        self.status = status

        Job.all_jobs[self.id] = self

    def to_influxdb(self):
        project = self.pipeline.project.full_path.replace('.', '_').replace('/', '.')
        stage = self.stage.replace('.', '_')
        namespace = f'jobs.{project}.{self.pipeline.id}.{stage}.{self.id}'
        print(namespace, 'created', self.created_at)
        if self.started_at is not None:
            print(namespace, 'started', self.started_at)
        if self.finished_at is not None:
            print(namespace, self.status, self.finished_at)

    @classmethod
    def new_job(cls, pipeline, data):
        id = data['id']
        stage = data['stage']
        created_at = data['created_at']
        started_at = data['started_at']
        finished_at = data['finished_at']
        status = data['status']

        return Job(id, pipeline, stage, created_at, started_at, finished_at, status)


class Pipeline(object):
    all_pipelines = {}

    def __init__(self, id, project, created_at, started_at, finished_at, status):
        self.id = int(id)
        self.project = project
        self.jobs = {}
        self.created_at = get_correct_round_time(created_at)
        self.started_at = get_correct_round_time(started_at)
        self.finished_at = get_correct_round_time(finished_at)
        self.status = status

        Pipeline.all_pipelines[self.id] = self

    def add_job(self, data):
        j = Job.new_job(self, data)
        self.jobs[j.id] = j

    def to_influxdb(self, start_time):
        for job in sorted(self.jobs):
            j = self.jobs[job]
            if j.created_at < start_time:
                continue
            j.to_influxdb()

    @classmethod
    def new_pipeline(cls, project, data):
        id = data['id']
        created_at = data['created_at']
        started_at = data['started_at']
        finished_at = data['finished_at']
        status = data['status']

        return Pipeline(id, project, created_at, started_at, finished_at, status)


class Project(object):
    all_projects = {}

    def __init__(self, id, full_path):
        self.id = int(id)
        self.full_path = full_path
        self.pipelines = {}

        Project.all_projects[self.id] = self

    def add_pipeline(self, data):
        p = Pipeline.new_pipeline(self, data)
        self.pipelines[p.id] = p

    def to_influxdb(self, start_time):
        for pipeline in sorted(self.pipelines):
            self.pipelines[pipeline].to_influxdb(start_time)


def round_time(t, step=3600):
    ts = int(t.timestamp())
    ts -= ts % step
    return datetime.fromtimestamp(ts)


class OneHourOfData(object):
    objects = {}

    def __init__(self, timestamp):
        self.timestamp = timestamp
        self.slots = dict([(step * i, {}) for i in range(int(3600 / step))])
        OneHourOfData.objects[timestamp] = self

    @classmethod
    def get_data_object(cls, timestamp):
        # not sure why, but `cls.objects.get(timestamp, OneHourOfData(timestamp))` fails
        # so manually deal with the key
        if timestamp in cls.objects:
            return cls.objects[timestamp]
        return OneHourOfData(timestamp)

    @classmethod
    def send_to_influxdb(cls):
        points = []
        for timestamp, obj in cls.objects.items():
            for offset, objects in obj.slots.items():
                ts = datetime.fromtimestamp(timestamp + offset).isoformat()
                for obj in objects.values():
                    obj['time'] = ts
                    print(ts, obj['tags'], bytes_to_human(obj['fields']['value']))

                    points.append(obj)
        idb.write_points(points)
        cls.objects = {}

    def add_data(self, measurement, tags, fields, ts):
        slot_key = (ts - ts % step) % 3600

        hour = ts - ts % 3600
        if hour != self.timestamp:
            raise Exception('wrong object')

        slot = self.slots[slot_key]

        item = slot.get(str(tags), {
            'measurement': measurement,
            'tags': tags,
            'fields': {},
        })
        for k, v in fields.items():
            item['fields'][k] = item['fields'].get(k, 0) + v

        slot[str(tags)] = item


def setup_geoip_db(geodb):
    global geoipdb, states_data, countries_data, countries

    geoipdb = geoip2.database.Reader(geodb)

    # construct the map US state/country name -> 2 letters worldmap code
    for kind, glob_obj in (('states', states_data), ('countries', countries_data)):
        filepath = Path(f'{kind}.yaml')
        if not filepath.exists():
            data = requests.get(f'https://raw.githubusercontent.com/grafana/worldmap-panel/master/src/data/{kind}.json').json()
            glob_obj = dict([(s['name'], s) for s in data])

            # special case where we get 'District of Columbia' instead of 'Washington DC'
            if 'Washington DC' in glob_obj:
                glob_obj['District of Columbia'] = glob_obj['Washington DC']

            # now compute the geohash tags for the various lat/long
            for entry in glob_obj.values():
                entry['geohash'] = geohash.encode(entry['latitude'], entry['longitude'], 8)

            with open(filepath, 'w') as f:
                yaml.safe_dump(glob_obj, f)
        else:
            with open(filepath) as f:
                glob_obj.update(yaml.safe_load(f))

    countries = dict(((s['key'], s) for s in countries_data.values()))

    # fix South Sudan
    countries['SS'] = countries['SD']


# equinix Ips can be retrieved through refresh_packet_ips.sh and are stored in a secret
dest_regexes = [
    ('packet', re.compile(r'^147.(75|28).(50|51|129).(173|57])$')),
    ('packet_arm', re.compile(r'^147.28.151.\d*$')),
    ('packet_ipv6', re.compile(r'^2604:1380:.*$')),
    ('hetzner', re.compile(r'^95.21[67].*')),
    ('hetzner_ipv6', re.compile(r'^2a01:4f9:4a:318d:.*$')),
    ('hetzner_ipv6', re.compile(r'^2a01:4f8:.*$')),
    ('local', re.compile(r'^(34.74.69.21|35.243.208.232)$')),
]


def get_geo_tags(dest_ip):
    dest = 'internet'

    if dest_ip in known_ips:
        dest = known_ips[dest_ip]['name']
    else:
        for k, r in dest_regexes:
            if r.match(dest_ip):
                dest = k

    tags = {'dest': dest}

    try:
        geo = geoipdb.city(dest_ip)
    except geoip2.errors.AddressNotFoundError:
        c = 'AQ'  # Antarctica
        tags['geohash'] = countries[c]['geohash']
        tags['geo_code'] = countries[c]['key']
        tags['country'] = countries[c]['name']
        print(f'ERROR: ip {dest_ip} not found in database', file=sys.stderr)
    else:
        if geo.country.iso_code == 'US':
            geo_tag = geo.subdivisions.most_specific.name
            tags['geohash'] = states_data[geo_tag]['geohash']
            tags['geo_code'] = states_data[geo_tag]['key']
            tags['country'] = geo.country.name
        else:
            if geo.country.iso_code not in countries:
                c = 'AQ'  # Antarctica
                tags['geohash'] = countries[c]['geohash']
                tags['geo_code'] = countries[c]['key']
                tags['country'] = countries[c]['name']
                print(f'ERROR: country {geo.country} not found in database', file=sys.stderr)
            else:
                tags['geohash'] = countries[geo.country.iso_code]['geohash']
                tags['geo_code'] = geo.country.iso_code
                tags['country'] = countries[geo.country.iso_code]['name']

    return tags


total = 0


def inject_gcp_csv_to_influxdb(csvfile, kind, last_object):
    '''parse a given csv file object, and inject the data in
    the influxDB time series db'''

    global total

    data = csv.DictReader(csvfile, delimiter=',', quotechar='"')

    prefix = f'fdo-gitlab-{kind}'
    dataobj = OneHourOfData.get_data_object(get_blob_ts(csvfile, f'{prefix}_usage_'))

    registry_regex = re.compile(r'/fdo-gitlab-registry/docker/registry/v2/repositories/(.*)/_manifests/.*/link')

    for row in data:
        log_point = namedtuple('LogPoint', row.keys())(*row.values())
        if log_point.cs_method != 'GET' or log_point.cs_operation != 'GET_Object':
            continue

        if log_point.cs_bucket != prefix:
            print(row)
            continue

        ts = int(log_point.time_micros[:-6]) + 3600  # convert to localtime

        sc_bytes = int(log_point.sc_bytes)

        tags = get_geo_tags(log_point.c_ip)

        total += sc_bytes
        if kind == 'registry':
            if '_manifests' in log_point.cs_uri:
                m = registry_regex.match(log_point.cs_uri)
                if m is None:
                    print(row)
                    continue

                project_image = m.group(1)

                last_object = project_image

            if last_object is None:
                print(row)
                continue

            namespace, project = last_object.split('/', 1)
            image = 'default'
            if '/' in project:
                project, image = project.split('/', 1)

            r_tags = {'kind': kind, 'namespace': namespace, 'project': project, 'image': image}

            dataobj.add_data('sent_bytes_sum', {**tags, **r_tags}, {'value': sc_bytes}, ts)

        elif kind == 'artifacts':
            obj = log_point.cs_object.split('/')
            name = obj[-1].replace('.', '_')
            job = obj[-3]

            try:
                project = Job.all_jobs[int(job)].pipeline.project.full_path
            except ValueError:
                continue
            except KeyError:
                project = 'unknown/project'

            namespace, project = project.split('/')

            a_tags = {'kind': kind, 'namespace': namespace, 'project': project, 'name': name}

            dataobj.add_data('sent_bytes_sum', {**tags, **a_tags}, {'value': sc_bytes}, ts)
        else:
            dataobj.add_data('sent_bytes_sum', {**tags, 'kind': f'{kind}'}, {'value': sc_bytes}, ts)

    return last_object


def get_blob_ts(blob, prefix):
    blob_date = blob.name[len(prefix):][:19]
    blob_datetime = datetime.strptime(blob_date, '%Y_%m_%d_%H_%M_%S')
    blob_ts = int(blob_datetime.timestamp()) + 3600  # convert to localtime

    return blob_ts


def skip_blob(blob, prefix, start=None, offset=0):
    blob_ts = get_blob_ts(blob, prefix)

    if start is None:
        start = start_timestamp

    return blob_ts < start - offset


def load_ci_pipelines(blob, force):
    if download_blob_if_needed(blob) or force:
        with open(blob.name) as csvfile:
            data = csv.DictReader(csvfile, delimiter=',', quotechar='"')
            for row in data:
                project = int(row['project_id'])

                try:
                    project = Project.all_projects[project]
                except KeyError:
                    try:
                        project = Project(project, projects[project])
                    except KeyError:
                        # project has been deleted
                        continue

                project.add_pipeline(row)


def load_ci_jobs(blob, force):
    if download_blob_if_needed(blob) or force:
        with open(blob.name) as csvfile:
            data = csv.DictReader(csvfile, delimiter=',', quotechar='"')
            for row in data:
                pipeline_id = int(row['pipeline_id'])
                try:
                    pipeline = Pipeline.all_pipelines[pipeline_id]
                except KeyError:
                    print(f'pipeline {pipeline_id} not found')
                else:
                    pipeline.add_job(row)


def load_data(bucket, kind, data_store, fields, force):
    blob_name = f'ci_{kind}_dump.csv'
    blob = bucket.get_blob(blob_name)

    if download_blob_if_needed(blob) or force:
        with open(blob.name) as csvfile:
            data = csv.DictReader(csvfile, delimiter=',', quotechar='"')
            for row in data:
                data_store[int(row['id'])] = "/".join([row[f] for f in fields])


def load_pipelines_jobs_data(bucket, debug_prefix, start_time=None, offset=0, force=False):
    for blob in bucket.list_blobs(prefix='ci_pipelines_dump-'):
        if skip_blob(blob, 'ci-pipelines_dump-', start=start_time, offset=offset):
            continue

        print(f'{debug_prefix} {blob.name}')

        load_ci_pipelines(blob, force)

    for blob in bucket.list_blobs(prefix='ci_jobs_dump-'):
        if skip_blob(blob, 'ci_jobs_dump-', start=start_time, offset=offset):
            continue

        print(f'{debug_prefix} {blob.name}')

        load_ci_jobs(blob, force)


def load_db(bucket, force):
    # load all the jobs/projects associations we have
    for kind, data_store, fields in [
        ('users', users, ('name',)),
        ('runners', runners, ('description',)),
        ('projects', projects, ('namespace', 'path')),
    ]:
        load_data(bucket, kind, data_store, fields, force)

    now = datetime.now()

    year = now.year if now.month > ci_dump_months_offset else now.year - 1
    month = now.month - ci_dump_months_offset
    if month <= 0:
        month += 12
    start_ci_dump = datetime(year, month, 1).timestamp()

    load_pipelines_jobs_data(bucket, 'local:', start_time=start_ci_dump, force=force)


def cleanup_storage(kinds):
    start_time = datetime.now()

    cleanup_ts = (start_time - timedelta(days=2)).timestamp()

    for value in kinds:
        prefix = f'fdo-gitlab-{value}_usage'

        p = Path('.')
        for i in p.glob(f'{prefix}*'):
            ts = get_blob_ts(i, f'{prefix}_')

            if ts < cleanup_ts:
                print(f'cleaning up {i.name}')
                i.unlink()


def parse_logs(bucket, kinds, last_object, last_times, force, dry_run):
    global total

    cleanup_storage(kinds)

    for value in kinds:
        prefix = f'fdo-gitlab-{value}_usage'
        last_time = last_times[value]
        for blob in bucket.list_blobs(prefix=prefix):
            if skip_blob(blob, f'{prefix}_', start=last_time):
                continue

            print(blob.name)

            ts = get_blob_ts(blob, f'{prefix}_')

            if last_time is None:
                last_time = ts

            if ts != last_time:
                # different day, send our data
                OneHourOfData.send_to_influxdb()
                last_times[value] = last_time

            last_time = ts

            if not dry_run:
                if download_blob_if_needed(blob) or force:
                    with open(blob.name) as csvfile:
                        last_object[value] = inject_gcp_csv_to_influxdb(csvfile, value, last_object[value])

        OneHourOfData.send_to_influxdb()
        print(f'grand total: {total} -> {bytes_to_human(total)}')
        # note: we do not store the last timestamp sent, in case we don't have all blobs available
        # those that are already downloaded will be ignored

        total = 0


@click.command()
@click.option('--dry-run', default=False, is_flag=True, help='Do not fetch the sql DB nor create/delete files.')
@click.option('--force', default=False, is_flag=True, help='force re-parsing previous data')
@click.option('--start-time', default=str(start_time), help='since when do we need to start')
@click.option('--kind', multiple=True, default=['artifacts', 'lfs', 'registry', 'uploads'], help='kind of objects to parse (can be used multiple times)')
@click.option('--interval', default=SLEEP_INTERVAL, help='time to wait between 2 iterations. 0 means do one iteration only')
@click.option('--database', default='logs-influxdb.monitoring', help='the database to contact')
@click.option('--packet-ips', default=None, help='a json file which contains the list of ips of all of our machines')
@click.option('--geodb',
              default='dbip-city-lite-2021-03.mmdb',
              type=click.Path(file_okay=True, dir_okay=False),
              help='the geoip database')
def main(dry_run, start_time, kind, force, interval, database, geodb, packet_ips):
    global start_timestamp, idb

    idb = influxdb.InfluxDBClient(database, 8086, 'root', 'root', DB_NAME)

    # ensures the database exists
    if DB_NAME not in [db['name'] for db in idb.get_list_database()]:
        idb.create_database(DB_NAME)

    # kind is an array, let's add an 's'
    kinds = kind
    last_times = dict([(k, None) for k in kinds])

    setup_geoip_db(geodb)

    start_time = round_time(datetime.fromisoformat(start_time), step)
    start_timestamp = start_time.timestamp()

    client = storage.Client.from_service_account_json('/etc/creds/upload_gcs_key')
    bucket = storage.Bucket(client, 'fdo-gitlab-storage-transfer-logs')

    # fetch packet informations
    if packet_ips is not None:
        with open(packet_ips) as f:
            ips = json.load(f)
            for name, data in ips.items():
                for ip in data['ip']:
                    known_ips[ip] = {
                        'name': name,
                    }
                    for k,v in data.items():
                        if k != 'ip':
                            known_ips[ip][k] = v

    last_object = dict([(k, None) for k in kinds])

    cleanup_storage(kinds)

    load_db(bucket, True)
    parse_logs(bucket, kinds, last_object, last_times, force, dry_run)

    if interval:
        while True:
            print(last_times)

            time.sleep(interval)
            load_db(bucket, False)
            parse_logs(bucket, kinds, last_object, last_times, force, dry_run)


if __name__ == '__main__':
    main()
